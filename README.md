# Description:

This is an exploratory repository for the Elixir Programming Language. This repository aims to dabble in different aspects that Elxir steps into as well as a learning experience for me and anyone who would peep this. I will do my best to document everything and either in the parent level or the accompanying child level, I aim to write my thoughts, explanations and future learning objectives in LaTeX and PDF files.

# Stats: 

Cups of Coffee Drank For This Repo So Far:

![Cups of Coffee](https://img.shields.io/badge/Coffee-1-blue.svg)


Cups of Tea Drank For This Repo So Far:

![Cups of Tea](https://img.shields.io/badge/Tea-1-blue.svg)

# License: 

[![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg?style=plastic)]()

Everything in this repository is released under the [MIT License](http://opensource.org/licenses/MIT).